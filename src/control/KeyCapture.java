package control;


import display.Main;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import model.Model;

/**
 * This class handles KeyBoard input. Every time a key is pressed, it is saved to the model and updates main.
 */

public class KeyCapture implements EventHandler<KeyEvent>{

    private Model model;
    private Main main;

    public KeyCapture (Model m, Main main){
        model = m;
        this.main = main;
    }

    /**
     * KEY_TYPED gets triggered on a character be registered (saves the character to the model)
     * KEY_PRESSED gets triggered for backspace being pressed (also saved to model)
     */
    @Override
    public void handle(KeyEvent event) {
        if (event.getEventType() == KeyEvent.KEY_TYPED){
            String input = event.getCharacter();
            model.addToCurrentPassage(input);
        }else if (event.getEventType() == KeyEvent.KEY_PRESSED) {

            if (event.getCode() == KeyCode.BACK_SPACE)
                model.deleteLastCharOfCurrentPassage();
            else if (event.getCode() == KeyCode.LEFT)
                model.moveCursor(-1);
            else if (event.getCode() == KeyCode.RIGHT)
                model.moveCursor(1);
            else if (event.getCode() == KeyCode.UP)
                model.moveLine(-1);
            else if (event.getCode() == KeyCode.DOWN)
                model.moveLine(1);
            else if (event.getCode() == KeyCode.ENTER)
                model.addToCurrentPassage("\n");
        }

        main.updateEntry();

    }


}
