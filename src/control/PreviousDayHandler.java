package control;

import display.Main;
import model.Model;

/**
 * Created by panduKerr on 21/5/17.
 */
public class PreviousDayHandler extends NextDayHandler {
    public PreviousDayHandler (Model m , Main main) {
        super(m,main);
    }

    @Override
    void handleAction (){
        model.getPrevious();
        main.updateEntry();
    }
}
