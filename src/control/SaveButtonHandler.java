package control;

import display.Main;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import model.Model;

/**
 * Created by panduKerr on 21/5/17.
 *
 * Handles saving to the model
 */
public class SaveButtonHandler implements EventHandler <MouseEvent> {
    private Model model;

    public SaveButtonHandler(Model mod) {
        model = mod;
    }


    /**
     *Indicates to the model that it must save its current Entry
     */
    @Override
    public void handle(MouseEvent event) {
        if (event.getEventType() == MouseEvent.MOUSE_CLICKED) {
            model.saveCurrent();
        }
    }
}
