package control;

import display.Main;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import model.Model;

/**
 * Created by panduKerr on 21/5/17.
 */
public class NextDayHandler implements EventHandler <MouseEvent>{
    Model model;
    Main main;

    public NextDayHandler (Model m, Main main) {
        model = m;
        this.main = main;
    }

    @Override
    public void handle(MouseEvent event) {
        if (event.getEventType() == MouseEvent.MOUSE_CLICKED){
            handleAction();
        }
    }

    /**
     * To be overridden subclasses
     */
    void handleAction () {
        model.getNext();
        main.updateEntry();
    }
}
