package display;

import com.sun.istack.internal.NotNull;
import display.GVI.*;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import model.Coordinates;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by panduKerr on 19/5/17.
 *
 * A skeleton describes the visible format of a journal entry. It formats the information given to it. It should
 * resemble a page from a journal. It describes a single state of a visible entry.
 */
 class Skeleton {
    private final double X_LENGTH;
    private final double Y_LENGTH;
    private final double X_MARGIN;

    private final Coordinates HEADER_COORD;
    private TextComponent header;
    private JfxGroup paragraph;

    private final Coordinates MAIN_PASSAGE_COORD;
    TextComponent mainPassage;



    VisualGroup visualGroup;



    Skeleton(double x_length, double y_length,  Date date, String main_passage) {


        //Sets the boundaries for the page
        X_LENGTH = x_length;
        Y_LENGTH = y_length;
        X_MARGIN = 25;

        //Sets coordinates for text boxes
        HEADER_COORD = new Coordinates(X_MARGIN,25);
        MAIN_PASSAGE_COORD = new Coordinates(X_MARGIN, 75);


        //Generates the text boxes
        generateHeader(date);
//        generateMainPassage(main_passage);
        paragraph = new JfxGroup();
//        paragraph.add(parse(main_passage));


        //Instantiates VisualGroup, adding the textBoxes to it
        visualGroup = new JfxGroup();
        visualGroup.add(header.getVisual());
//        visualGroup.add(mainPassage.getVisual());
        visualGroup.add(paragraph);

        updateMainPassage(main_passage,main_passage.length());

    }

    /**
     * A header is today's date. This sets headerString to a formatted String representing today's date. This method
     * updates the TextComponent header
     */
    private void generateHeader(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("EEE dd MMM");

        VisualText emptyVisText = new JfxText(HEADER_COORD.x,HEADER_COORD.y,"");

        header = new TextComponent(emptyVisText,format.format(date));
    }

    /**
     * The main passage is where the user writes about their day. It displays what they've already written
     *
     * It formats the passage, wrapping the text. It inserts a '|' character at the end, representing the cursor.
     *
     * It parses a string into a char array. Each char is used to make
     */
     private void generateMainPassage (String passage) {
        //Find lineLength in pixels
        double doubleLineLength = (X_LENGTH - 2 * X_MARGIN);
        int lineLength = (int) doubleLineLength;


        VisualText emptyVisText = new JfxText(MAIN_PASSAGE_COORD.x,MAIN_PASSAGE_COORD.y,passage);
        emptyVisText.setWrap(lineLength);
        //Adds "|"  to the end of passage, representing the cursor
        mainPassage = new TextComponent(emptyVisText, passage + "|");
    }

    /**
     * Refreshes the passage. Inserts the '|' character in front of the model's cursor. Note mainPassage refers to the
     * text component. This class has no access to the actual entry
     */
    void updateMainPassage (String newPassage, int cursor){
//        StringBuilder stringBuilder = new StringBuilder(newPassage);
//        newPassage = stringBuilder.insert(cursor,"|").toString();
//        mainPassage.setString(newPassage);
        DynamicTextParser d = new DynamicTextParser(newPassage,X_MARGIN,75,30);

        paragraph.getWrappedVisible().getChildren().clear();
        //Makes empty VBox to pass to the DynamicTextParser

        //Remember paragraph.getWrappedVisible() is a group
        paragraph.getWrappedVisible().getChildren().add(d.parse());
        System.out.println("here 1");

    }

    void updateHeader (Date date) {
        SimpleDateFormat sd = new SimpleDateFormat("EEE dd MMM");
        String toDisplay = sd.format(date);
        header.setString(toDisplay);
    }




//    public static void main(String[] args) {
//        Skeleton skeleton = new Skeleton(100,100,new Date(), "tt");
//        VBox test = skeleton.parse("FFFff fnjknknkjn jknkjn jnkj nj kn k    \n jnnnjkn ");
//        HBox line1 = (HBox) test.getChildren().get(0);
//        System.out.println(line1.getChildren().size());
//        for (Node n : line1.getChildren()) {
//            //Each n represents a word
//            HBox m = (HBox) n;
//            String tester = "";
//            for (Node p: m.getChildren()) {
//                //each p represent a Text representing a letter
//                Text q = (Text) p;
//
//                tester += q.getText();
//            }
//            System.out.println(tester.toString().toString().toString().toString());
//        }
//    }






}
