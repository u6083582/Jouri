package display.GVI;

import javafx.scene.Group;
import javafx.scene.Node;

/**
 * Created by panduKerr on 14/5/17.
 *
 * Javafx implementation of the VisualGroup interface.
 */
public class JfxGroup extends VisualGroupAbstract {
    Group group;

    public JfxGroup() {
        group = new Group();
    }

    /**
     * Only access to wrapped group
     * @return javafx Group
     */
    @Override
    public Group getWrappedVisible() {
        return group;
    }

    /**
     * Adds the object returned by v.getVisual to the group.
     *  Note: This' does not actually save a reference to v.
     *
     * @param v - v must extend visualAbstract. v.getVisual must return a javafx.scene.Node
     */
    @Override
    public void add (Visual v) {
        VisualAbstract va = (VisualAbstract) v;
        Node n = (Node) va.getWrappedVisible();
        group.getChildren().add(n);
    }

    /**
     * Ugly add - does not follow normal structure.
     */
    public void add (Node n) {
        group.getChildren().add(n);
    }

    /**
     * @param v - Must extend VisualGroupAbstract - v.getVisual must return a javafx.scene.Node
     */
    @Override
    public void add(VisualGroup v) {
        VisualGroupAbstract vga = (JfxGroup) v;
        Group g = (Group) vga.getWrappedVisible();
        group.getChildren().add(g);
    }

    @Override
    public String toString() {
        return "Hi this is a JfxGroup";
    }

    public void clear (){
        group.getChildren().clear();
    }

}
