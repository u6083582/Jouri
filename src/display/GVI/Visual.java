package display.GVI;

import javafx.scene.paint.Color;

/**
 * Created by panduKerr on 14/5/17.
 *
 * Interface for interaction with visible items. This maintains the Control - Model - View Structure. The class that
 *  implements this interface wraps and hides the visible item from the model, allowing it only to be interpreted by the
 *  display module.
 *
 * No classes should directly implement this, they should instead extend VisualAbstract. This interface is for the
 *  convenience of model module
 */
public interface Visual {

    /**
     * Implementation should set this visual's internal visible object to other's, rather than something
     *  like: this = other.
     * @param other the other Visual of same class
     */
    void setVisual (Visual other);


    /**
     * Should rotate internal visible object
     * @param n - degrees to rotate
     */
    void rotate (double n);


    /**
     * @return top, left hand corner coordinates of the wrapped visible object
     */
    double getX ();
    double getY ();

    /**
     * Sets the top, left hand corner coordinates of the wrapped visible object
     */
    void setX (double x);
    void setY (double y);

    /**
     * Sets wrapped object's fill to a javafx.scene.paint.Color
     */
    void setFill (Color color);

}

