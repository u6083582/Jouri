package display.GVI;


/**
 * Created by panduKerr on 14/5/17.
 *
 * VisualGroup acts as an interface between the model and display modules.
 * Interface for a group of visual items to be passed around in the Model.
 * Ideally nothing in the model module should be able to interpret the content.
 *
 * No classes should directly implement this, they should instead extend VisualGroupAbstract. This interface is for the
 * convenience of model module
 *
 */
public interface VisualGroup {

    /**
     * Typically for use within Model module
     * @param v - a Visual or VisualGroup whose content can only be interpreted by the display module
     */
    void add (Visual v);
    void add (VisualGroup v);

    /**
     * Can be called by both model and display modules
     */
    void clear ();
}
