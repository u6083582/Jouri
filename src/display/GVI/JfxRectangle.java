package display.GVI;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * Created by panduKerr on 14/5/17.
 *
 * Javafx implementation of JfxRectangle
 */
public class JfxRectangle extends VisualAbstract {
    Rectangle rectangle;

    public JfxRectangle(double x, double y, double width, double height){
        rectangle = new Rectangle(x,y,width,height);
    }

    public JfxRectangle(double x, double y, double width, double height, Color color){
        rectangle = new Rectangle(x,y,width,height);
        rectangle.setFill(color);
    }


    @Override
    public double getX() {
        return rectangle.getX();
    }

    @Override
    public double getY() {
        return rectangle.getY();
    }

    @Override
    public Object getWrappedVisible() {
        return rectangle;
    }


    @Override
    public void rotate(double n) {
        rectangle.setRotate(n);
    }

    @Override
    public void setX(double x) {
        rectangle.setX(x);
    }

    @Override
    public void setY(double y) {
        rectangle.setY(y);
    }

    @Override
    public void setFill(Color color) {
        rectangle.setFill(color);
    }

    /**
     * @param v must extend VisualAbstract. It also must wrap javafx.scene.shape.Rectangle
     */
    @Override
    public void setVisual(Visual v) {
        VisualAbstract vga = (VisualAbstract) v;
        this.rectangle = (Rectangle) ( vga.getWrappedVisible());
    }

    @Override
    public String toString() {
        return "This is a JfxRectangle. Hi.";
    }
}
