package display.GVI;

import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 * Created by panduKerr on 19/5/17.
 */
public class JfxText extends VisualAbstract implements VisualText {

    private Text jfText;


    public JfxText (double x, double y, String s) {
        setX(x);
        setY(y);

        jfText = new Text(getX(),getY(),s);

    }

    @Override
    public void setFill(Color color) {
        jfText.setFill(color);
    }

    @Override
    public Object getWrappedVisible() {
        return jfText;
    }


    public void setText (String s){
        jfText.setText(s);
    }

    /**
     * @param v -must be class that wraps a javafx Text
     */
    @Override
    public void setVisual(Visual v) {
        VisualAbstract vab = (VisualAbstract) v;
        this.jfText = (Text) vab.getWrappedVisible();
    }

    @Override
    public void rotate(double n) {
        jfText.setRotate(n);
    }

    /**
     * Sets the length of the wrapped text
     * @param n - pixels to wrap
     */
    @Override
    public void setWrap (double n) {
        jfText.setWrappingWidth(n);
    }
}
