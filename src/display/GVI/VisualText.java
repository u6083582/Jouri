package display.GVI;

/**
 * Created by panduKerr on 20/5/17.
 *
 * Interface used to represent on screen text
 */
public interface VisualText extends Visual {

    /**
     * Sets the string literal of the object wrapped by the class that implements this interface
     * @param s
     */
    void setText (String s);

    /**
     * Determines the length of a line for wrapping
     */
    void setWrap (double n);

}
