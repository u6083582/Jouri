package display.GVI;

/**
 * Created by panduKerr on 15/5/17.
 * An abstract class that implements Visual: it ensures package private methods.
 * Any class wishing to implement VisualGroup should instead extend this class.
 */
public abstract class VisualAbstract implements Visual {

    private double x;
    private double y;

    @Override
    public double getY() {
        return y;
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public void setY(double y) {
        this.y = y;
    }

    @Override
    public void setX(double x) {
        this.x = x;
    }


    /**
     * @return the visible object wrapped by a subclass. This object should only be able to be read by the display
     *  module
     */
    public abstract Object getWrappedVisible();
}
