package display;

import control.KeyCapture;
import control.NextDayHandler;
import control.PreviousDayHandler;
import control.SaveButtonHandler;
import display.GVI.JfxGroup;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import model.Model;

public class Main extends Application {

    private Group root;
    private Model model;
    private KeyCapture keyControl;
    private SaveButtonHandler saveButtonHandler;
    private NextDayHandler nextDayHandler;
    private PreviousDayHandler previousDayHandler;
    private Skeleton main_skeleton;
    private final double X_LENGTH = 400;
    private final double Y_LENGTH = 500;
    private final int CHARACTERS_PER_LINE = 40;

    //Interface items
    private Rectangle saveButton;
        private final double SAVE_X = 24;
        private final double SAVE_Y = 37;
        private final double SAVE_X_SIZE = 30;
        private final double SAVE_Y_SIZE = 15;


    private Rectangle prevButton;
        private final double PREV_X = 167;
    private Rectangle nextButton;
        private final double NEXT_X = 215;
        private final double NAV_Y  = 470;
        private final double NAV_X_SIZE = 22;
        private final double NAV_Y_SIZE = 17;


    @Override
    public void start(Stage primaryStage) throws Exception{
        //Instantiates root and model
        root = new Group();
        model = new Model(CHARACTERS_PER_LINE);

        //Instantiates control classes
        keyControl = new KeyCapture(model, this);
        saveButtonHandler = new SaveButtonHandler(model);
        previousDayHandler = new PreviousDayHandler(model, this);
        nextDayHandler = new NextDayHandler(model,this);

        //Sets up stage
        primaryStage.setTitle("Experiment 4");
        Scene main_scene = new Scene(root, X_LENGTH, Y_LENGTH);
        primaryStage.setScene(main_scene);


        //loads today's entry, sets up formatting Skeleton
        loadEntry();



        //Instantiates GUI items
        //SaveButton
        saveButton = new Rectangle(SAVE_X, SAVE_Y,SAVE_X_SIZE,SAVE_Y_SIZE);
            saveButton.setFill(Color.LIGHTGRAY);
            saveButton.setArcHeight(10);
            saveButton.setArcWidth(10);
            saveButton.setOnMouseClicked(saveButtonHandler);
            //Adds save button to root
            root.getChildren().add(saveButton);

        //prevButton
        prevButton = new Rectangle(PREV_X, NAV_Y, NAV_X_SIZE, NAV_Y_SIZE);
            prevButton.setFill(Color.LIGHTGRAY);
            prevButton.setArcWidth(10);
            prevButton.setArcHeight(10);
            prevButton.setOnMouseClicked(previousDayHandler);
            root.getChildren().add(prevButton);

        //nextButton
        nextButton = new Rectangle(NEXT_X, NAV_Y, NAV_X_SIZE, NAV_Y_SIZE);
            nextButton.setFill(Color.LIGHTGRAY);
            nextButton.setArcWidth(10);
            nextButton.setArcHeight(10);
            nextButton.setOnMouseClicked(nextDayHandler);
            root.getChildren().add(nextButton);

        //Adds keyboard event handlers
        main_scene.setOnKeyTyped(keyControl);
        main_scene.setOnKeyPressed(keyControl);
        primaryStage.show();


    }

    /**
     * Loads data into the root node. The data is first formatted through a Skeleton
     */
    void loadEntry (){
        root.getChildren().clear();

        main_skeleton = new Skeleton(X_LENGTH,Y_LENGTH,model.getCurrentsDate() ,model.getCurrentsPassage());

        JfxGroup skelGroup = (JfxGroup) main_skeleton.visualGroup;
        root.getChildren().add(skelGroup.getWrappedVisible());
    }

    /**
     * Re-accesses module's current entry's, and redraw the text box. Call after making any change to the currentEntry
     */
    public void updateEntry() {
        main_skeleton.updateMainPassage(model.getCurrentsPassage(),model.getCursor());
        main_skeleton.updateHeader(model.getCurrentsDate());
    }

    /**
     * On quit saves currentEntry.
     * @throws Exception
     */
    @Override
    public void stop() throws Exception {
        model.saveCurrent();
        super.stop();
    }

    //    public static void main(String[] args) {
//        launch(args);
//    }
}
