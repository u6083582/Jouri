package display;

import com.sun.istack.internal.NotNull;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/**
 * Created by panduKerr on 22/5/17.
 *
 * Due to limitations with Javafx, we don't know the layout bounds until shown, so the parsing must show the results as
 * it goes. So this parses a section, returns it to be displayed and then parses the next section
 */
public class DynamicTextParser {
    final private double X_MARGIN;
    final private double Y_MARGIN;
    final private double CHARS_PER_LINE;
    private ArrayList <String []> toBeParsed;
    //Contains all lines to be displayed.
    private VBox paragraph;
    Boolean completed = false;


    DynamicTextParser (String toBeParsed, double xMargin, double yMargin, double charsPerLine){

        X_MARGIN = xMargin;
        Y_MARGIN = yMargin;
        CHARS_PER_LINE = charsPerLine;

        paragraph = new VBox();
        paragraph.setLayoutX(X_MARGIN);
        paragraph.setLayoutY(Y_MARGIN);
        //Parse passage into words and whitespaces
        //First parses String into words
        String [] parsedWords = parseIntoWords(toBeParsed.toCharArray());
        //Next parses words into an array of lines, limited by CHARS_PER_LINE
        this.toBeParsed = new ArrayList<>(Arrays.asList(parseWordsIntoLines(parsedWords)));
        completed = false;
    }

//
//    /**
//     * Parses a section (AKA a word). Each section must be shown, then this must be called again so this can accurately
//     * gauge the size of each line
//     *
//     * @param previousOutput - must be previous output from this function. The idea is for the caller function to call
//     *                       this method in a while loop, while (!completed) and get parse's output, display it and call
//     *                       parse again, giving it back its old output. This is because in Javafx, when a pane is shown
//     *                       its layoutBounds is set, allowing us to read a line's width
//     */
//    VBox parse (VBox previousOutput){
//        //if, on last parse, the last line of paragraph is longer than CHARS_PER_LINE, it puts the last word (which
//        // caused the last line to be too long) into a new line
//        alignParagaph();
//
//        paragraph = previousOutput;
//        paragraph.setLayoutX(X_MARGIN);
//        paragraph.setLayoutY(Y_MARGIN);
//
//        //This means that this parse call will be the final one, returning the last element of toBeParsed
//        if (toBeParsed.size() == 1)
//            completed = true;
//
//        parse();
//
//        return paragraph;
//    }






    /**
     * Pasrses a string as follows. Produces an array of Javafx Texts, each containing one char, and then puts these
     * into an HBox, representing a word. These words are then put into another HBox, representing a line. A collection
     * of lines is stored in a final VBox.
     *
     *
     * Parses the next String in toBeParsed. It then decreases the length of toBeParsed by 1 (excludes the just parsed
     * String).
     *
     */
     VBox parse(){

        VBox lines = new VBox();

        for (String[] line: toBeParsed) {

            //working represents an HBox where every child is a char of workingString wrapped by a Text
            HBox lineBox = new HBox();

            //Iterates through all words in line
            for (int i = 0; i < line.length; i++) {

                String word = line[i];
                char[] letters = word.toCharArray();

                HBox wordBox = new HBox();
                //Iterates through letters that make up word
                for (int j = 0; j < letters.length; j++) {
                    //Adds each character as a Text object into working

                    //if(letters[j] == '\n') //STRANGE LINE, CAN BREAK ALL CODE
                    //wordBox.getChildren().add(new Text(Character.toString(letters[j])));
                    wordBox.getChildren().add(new Text(Character.toString(letters[j])));
                }

                lineBox.getChildren().add(wordBox);
            }
            lines.getChildren().add(lineBox);

        }

         lines.setLayoutY(Y_MARGIN);
         lines.setLayoutX(X_MARGIN);

//                HBox lastLine;
//                if (paragraph.getChildren().size() <= 0)
//                    paragraph.getChildren().add(new HBox());
//                lastLine = (HBox) paragraph.getChildren().get(paragraph.getChildren().size() - 1);
//
//                lastLine.getChildren().add(working);



//        //Container for all lines, represented as a HBox
//        VBox lines = new VBox();
//        lines.getChildren().add(new HBox());
//
//
//        for (int i = 0; i < hboxWords.length; i++) {
//            //gets the last HBox in lines
//            HBox workingLine = (HBox) lines.getChildren().get(lines.getChildren().size() - 1);
//
//            //Tests if working line + the next HBox word can fit
//            workingLine.getChildren().add(hboxWords [i]);
//            System.out.println(workingLine.getLayoutBounds().getWidth());
//
//            if (workingLine.getWidth()> (CHARS_PER_LINE)) {
//                //if this new line cannot fit, pops the last element, adding it to lines
//                workingLine.getChildren().remove(workingLine.getChildren().size() - 1);
//                lines.getChildren().add(new HBox());
//                //gets newly made HBox
//                HBox newHbox = (HBox) lines.getChildren().get(lines.getChildren().size() - 1);
//                newHbox.getChildren().add(hboxWords[i]);
//            }
//        }

            //Decreases the length of toBeParsed by one, eliminating the just parsed element

        return lines;
    }


//    /**
//     * If the last HBox in paragraph has a length greater than the CHARS_PER_LINE, pop its last element and put it in a
//     * new HBox, which is added to the end of paragraph
//     */
//    private void alignParagaph (){
//
//        //If paragraph is empty (Aka on first parse) creates an empty line
//        if (paragraph.getChildren().size() == 0)
//            paragraph.getChildren().add(new HBox());
//
//        HBox lastLine = (HBox) paragraph.getChildren().get(paragraph.getChildren().size() - 1);
//
//        System.out.println(lastLine.getWidth());
//
//        if (lastLine.getLayoutBounds().getWidth() > CHARS_PER_LINE){
//            //gets last HBox from lastLine (represents final word, which is causing the line to be too long)
//            HBox lastWord = (HBox) lastLine.getChildren().get(lastLine.getChildren().size() - 1);
//            //removes the last HBox from lastLine
//            lastLine.getChildren().remove(lastLine.getChildren().size() - 1);
//            //Makes the new line, adding the lastWord
//            HBox newLine = new HBox();
//            newLine.getChildren().add(lastWord);
//            //Adds newLine to the end of paragraph
//            paragraph.getChildren().add(newLine);
//        }
//    }


    /**
     * Returns an array of subarrays - each subarray representing a line of characters. The number of characters is
     * limited by CHARS_PER_LINE
     */
    String [] [] parseWordsIntoLines (@NotNull String [] words){



        //Will be returned (after transformation to array)
        ArrayList <String [] > lines = new ArrayList<>();

        //line is stored here before transferred to lines
        ArrayList<String> workingLine = new ArrayList<>();

        for (String word: words) {
            //Tests the size of workingLine if word is to be added
            int workingLineTestLength = 0;


            for (String s: workingLine) {
                workingLineTestLength += s.length();
            }

            workingLineTestLength += word.length();

            if (word.equals(System.lineSeparator())){
                System.out.println("line separator detected");
                //If the word is a newLine character, add it to lines as its own line
                lines.add(workingLine.toArray(new String[0]));
                workingLine.clear();
                lines.add(new String [] {"\n"});
            }

            //If the new line, along with the new word is still under CHARS_PER_LINE, add it
            else if (workingLineTestLength <= CHARS_PER_LINE)
                workingLine.add(word);

            else if (workingLineTestLength > CHARS_PER_LINE && workingLine.size() > 1){
                //Adding it workingLineTestLength will be too long. This isn't the case of one really long word
                lines.add(workingLine.toArray(new String[0]));
                workingLine.clear();
                workingLine.add(word);
            }
            else {
                //User attempted to enter really long word. We must break it up. We must test the very last
                // element to see if it is the size of CHARS_PER_LINE, if so we must also put a line after it to prevent
                // errors
                lines.add(workingLine.toArray(new String[0]));

                ArrayList <String> splitStrings = breakBigWord(word);
                int index = 0;
                int finalIndex = splitStrings.size() - 1;

                for (String splitString: splitStrings) {

                    if(index != finalIndex){
                        lines.add(workingLine.toArray(new String[0]));

                    } else {
                    //We are examining final index

                        if (splitString.length() == CHARS_PER_LINE){
                            //We have to clear working line in this case, otherwise error
                            lines.add(workingLine.toArray(new String[0]));
                            workingLine.clear();
                        }
                        else {
                            workingLine.add(splitString);
                        }
                    }

                    index ++;
                }

            }
        }
        //Store the last line
        lines.add(workingLine.toArray(new String[0]));

        String [] [] linesArray = lines.toArray(new String [0] [0]);
//        System.out.println("----------------------");
//        for (int i = 0; i < linesArray.length; i++) {
//            System.out.println(Arrays.toString(linesArray[i]));
//        }


        return linesArray;
    }

    /**
     * Breaks a word longer than CHARS_PER_LINE into strings of CHARS_PER_LINE
     */
    ArrayList <String> breakBigWord (@NotNull  String s) {
        ArrayList <String> list = new ArrayList<>();
        int counter = 0;
        String workingString = "";
        char [] chars = s.toCharArray();

        for (int i = 0; i < chars.length; i++) {
            workingString += Character.toString(chars[i]);
            if (workingString.length() == CHARS_PER_LINE) {
                list.add(workingString);
                workingString = "";
                counter = 0;
            }
        }
        list.add(workingString);
        return list;
    }



    /**
     *This turns an array of chars to to an array of Strings, representing words. A word is a sequence of letters
     * separated by a series of spaces. This preserves whiteSpace. Each line of CHARS_PER_LINE or less is put into a
     * sub array -representing a line of text. Note that newline character is treated as its own word.
     * @return Array of strings representing words, and the spaces that separate them
     */
    String [] parseIntoWords(@NotNull char [] raw) {
        //To be turned into an array
        ArrayList<String> words = new ArrayList<>();

        //True if currently parsing a word. False if currently parsing whitespace
        boolean wordParsingState = false;

        //Holds words and whiteSpace until it is to be added to words
        String workingString = "";

        for (int i = 0; i < raw.length; i++) {
            //Parsing the first character. This method does not know if it should be in stringParsingState or not
            if(i == 0 ){
                //sets wordParsing state to true if raw[i] isn't a whitespace character
                wordParsingState = !(raw [i] == ' ' || raw [i] == '\n');
            }

            if (wordParsingState) {
                //If raw[i] is conforms to the current state, add raw[i] to workingString
                if (!(raw [i] == ' ' ))
                    workingString += Character.toString(raw[i]);
                //Note this sneaky else if: it adds a newline character as a separate string to words.
                //  it has no effect on the rest of the parser
                else if(raw [i] == '\n') {
                    System.out.println("here2");
                    words.add(workingString);
                    workingString = "";
                    words.add(Character.toString(raw [i]));

                }
                else {
                    // raw[i] is contrary to the current state. We set wordParsingState to false, then
                    // Store the workingString into words, clear workingString, and add raw[i] as its first character
                    words.add(workingString);
                    workingString = "";
                    workingString += Character.toString(raw[i]);
                    wordParsingState = false;
                }

            } else {
                //We are in the !wordParsingState

                //raw[i] conforms to the current state
                if (raw [i] == ' ')
                    workingString += Character.toString(raw [i]);
                else if(raw [i] == '\n') {
                    words.add(workingString);
                    workingString = "";
                    words.add(Character.toString(raw [i]));
                }
                else {
                    //raw[i] is contrary to the current string.
                    words.add(workingString);
                    workingString = "";
                    workingString += Character.toString(raw[i]);
                    wordParsingState =true;
                }
            }
        }
        //As the for loop only adds workingString to words only when wordParsing state changes, we have to manually add
        //  the last instance of workingString
        if (!workingString.equals(""))
            words.add(workingString);

        String [] wordArray = words.toArray(new String [0]);

        return wordArray;
    }


    public static void main(String[] args) {
        char [] test = {'\n','f','d'};
    }

}
