package display;

import display.GVI.VisualText;

/**
 * Created by panduKerr on 20/5/17.
 *
 * This class couples a VisualText with a String
 */

 class TextComponent {
    private VisualText visualText;
    private String contents;


    /**
     * Uses v.setText(s) in order to ensure v represents the String s.
     *
     * @param  v - implementation of VisualText. v does not need to represent the String s
      */
    TextComponent (VisualText v, String s) {
        visualText =v;
        setString(s);
    }

    /**
     * Updates the contents String and visualText's String
     */
    void setString (String s) {
        contents = s;
        visualText.setText(s);
    }

    /**
     *
     * @return visualText. It does not return what is wrapped by visualText
     * ,for that use visualText.getWrappedVisual()
     */
    VisualText getVisual (){
        return visualText;
    }

    @Override
    public String toString() {
        return contents;
    }
}
