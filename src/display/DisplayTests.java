package display;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Date;

/**
 * Created by panduKerr on 20/5/17.
 */
public class DisplayTests {
//    @Test
//    public void skeletonPassageFormat (){
//        String passage = "Hi my name is david and Im a pretty cool bloke";
//        Skeleton s = new Skeleton(100,100,new Date(),passage);
//        assertTrue("The newly formatted string should be longer than the input string, but isn't"
//                ,s.mainPassage.toString().length() > passage.length());
//    }

    @Test
    public void testParseIntoWordsNormal () {
        DynamicTextParser d = new DynamicTextParser ("fffff",4,4,4);
        String [] test = {"Hi"," ", "m"," ","ffff"};
        String testString = "";

        for (int i = 0; i < test.length; i++) {
            testString += test [i];
        }

        char [] input = testString.toCharArray();

        String [] result = d.parseIntoWords(input);

        assertTrue("Correct: " + Arrays.toString(test) + " result: "+Arrays.toString(result)
                , Arrays.equals(test,result));

    }

    @Test
    public void updateMainPassage () {
        Skeleton s = new Skeleton(500,500,new Date(),"THis is what we're going to di!");

    }

    @Test
    public void wordParser () {
        DynamicTextParser d = new DynamicTextParser(" ",4,4,6);
        String test = "VVV VVv \n vvv b";
        String [] correct = {"VVV", " ", "VVv", " ", "\n", " ", "vvv", " ", "b"};
        String [] result = d.parseIntoWords(test.toCharArray());
        assertTrue("correct: " + Arrays.toString(correct) + " result: " +Arrays.toString(result)
                ,Arrays.equals(result, correct));
    }

    @Test
    public void wordParser2 () {
        DynamicTextParser d = new DynamicTextParser(" ",4,4,6);
        String test = " \n ";
        String [] correct = {" ", "\n", " "};
        String [] result = d.parseIntoWords(test.toCharArray());
        assertTrue("correct: " + Arrays.toString(correct) + " result: " +Arrays.toString(result)
                ,Arrays.equals(result, correct));
    }

}
