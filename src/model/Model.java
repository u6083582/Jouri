package model;

import java.util.Date;

/**
 * Created by panduKerr on 19/5/17.
 *
 * Main class for the model module
 *
 * The journal works as a series of entries. Each entry corresponds to a different day. If the application is closed
 * and then reopened, the user will continue to edit the entry for that day.
 *
 * If a user doesn't use the journal on a certain day, no entry is kept.
 */
public class Model {
    private Journal journal;
    private Entry currentEntry;
    private int textCursor = 0;
    private int lineLength;

    public Model (int lineLength){
        journal = new Journal();
        loadFirst();
        this.lineLength = lineLength;
    }

    /**
     * @return position of cursor in the current Entry's main passage
     */
    public int getCursor (){
        return textCursor;
    }

    /**
     * loads an entry for given date
     * @param d - Date. If d doesn't exist, it won't load anything (keeping the same entry open)
     */
    public void loadEntry (Date d) {}

    /**
     * This loads default entry
     */
    public void loadFirst () {
        currentEntry = journal.getCurrentEntry();
        setCursorToEnd();
    }

    /**
     * @return the currently loaded entry's unformatted date
     */
    public Date getCurrentsDate (){
        return currentEntry.getDate();
    }

    /**
     * @return the currently loaded entry's unformatted passage
     */
    public String getCurrentsPassage (){
        return currentEntry.getMainPassage();
    }

    /**
     * @param s - String to be appended to the currently loaded passage.
     */
    public void addToCurrentPassage (String s) {
        //currentEntry.appendPassage(s);
        currentEntry.insertStringIntoMainPassageAt(textCursor, s);
        moveCursor(s.length());

        if (s.equals("\n")) System.out.println("ENTER DETECTED");
    }


    /**
     * Deletes the last char of Current's main passage
     */
    public void deleteLastCharOfCurrentPassage () {
        //When the text cursor is at 0, it refers to inserting an element before the first char in the string
        if (textCursor - 1 >= 0)
            //The text cursor is always in front of the char to be backspaced
            currentEntry.backSpace(textCursor-1);

        moveCursor(-1);
    }

    /**
     * Saves CurrentEntry in journal
     */
    public void saveCurrent () {
        journal.writeEntry(currentEntry);
    }

    /**
     * Sets currentEntry to the day before.
     */
    public void getPrevious () {
        saveCurrent();
        currentEntry = journal.getPreviousEntry();
        setCursorToEnd();
    }

    /**
     * sets currentEntry to the next day
     */
    public void getNext () {
        saveCurrent();
        currentEntry = journal.getNextEntry();
        setCursorToEnd();
    }

    /**
     * Sets the cursor to the end of current Entry's main passage
     */
    private void setCursorToEnd () {
        textCursor =currentEntry.getMainPassage().length();
    }


    /**
     * if the offset is larger than the mainPassage length, cursor is to the end.
     *
     * @param offset - offset from current position. Negative to decrease cursor by that amount, positive to increase
     *               cursor by that amount. If given an offset of 0, no effect.
     */
    public void moveCursor(int offset) {
        textCursor += offset;
        if (textCursor < 0)
            textCursor = 0;
        else if(textCursor > currentEntry.getMainPassage().length())
            setCursorToEnd();

    }

    /**
     * Moves the cursor offset number of lines (defined by lineLength).
     * @param offset - number of lines to move. if 0 has no effect
     */
    public void moveLine (int offset) {
        System.out.println(offset * lineLength);
        moveCursor(lineLength*offset);
    }

}
