package model;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by panduKerr on 20/5/17.
 *
 * This class holds and manages all the journal entries, forming a journal. This class holds an ArrayList of entries,
 * with the least indexes being the most recent. This class is to pull the entry data from storage.
 *
 */
 class Journal {
     //Stored with least indexes being the most recent
    private ArrayList <Entry> entries;
    //dateCursor referring to the current Entry being examined
    private int dateCursor = 0;


     Journal (){
         entries = new ArrayList <>();
         //Makes an entry for today if one doesn't exist
         getEntry2(new Date());
         //sets dateCursor to today's date
         dateCursor = getIndex(new Date());

         SimpleDateFormat sd = new SimpleDateFormat(Utility.dateCodeFormat);
         System.out.println("Todays date code: " + sd.format(new Date()));

    }

    /**
     * Loads all entries stored into entries. It is in order from most recent to oldest. Call this anytime something is
     * written to storage
     */
    private void loadAllEntries () {

        Storage s = new Storage();
        Entry [] entryArray = s.readAll();
        entries =  new ArrayList<>(Arrays.asList(entryArray));

        //If there are no entries, entries is unsafe to use
        if (entries.size() == 0) {
            Entry dummy = new Entry();
            s.write(dummy);
            //Now that an entry is written reload this method. entries.size will now be at least 1.
            //Warning: will enter infinite loop if Storage does not correctly write or retrieve the new entry
            loadAllEntries();
        }
    }

    /**
     * Writes an entry into storage. Creates a new file if one doesn't exist
     * @param e
     */
    void writeEntry (Entry e) {
        Storage s = new Storage();
        s.write(e);
        loadAllEntries();
    }

    /**
     * gets a specific entry for a day. If one doesn't exist, one is generated
     * @return Entry from storage
     */
    Entry getEntry2 (Date d) {
        Storage s = new Storage();
        String dateCode = Utility.generateDateCode(d);
        //generates the file if it doesn't exist
        s.read(dateCode);
        //Updates entries
        loadAllEntries();

        dateCursor = getIndex(d);

        return entries.get(dateCursor);
    }

    /**
     * Gets the dateCursor of entry referring to a certain date
     * @return dateCursor of an entry of a certain date. Returns -1 if it could not be found
     */
    private int getIndex (Date d){
        String dateCode = Utility.generateDateCode(d);
        int index = -1;
        int workingIndex = 0;

        for (Entry e: entries) {
            if (e.getDateCode().equals(dateCode)){
                index = workingIndex;
                break;
            }
            workingIndex ++;
        }
        return  index;
    }

    /**
     * Gets the entry of the day directly after the day indicated by the dateCursor. If one doesn't exist one is made.
     * It works even if the dateCursor is on today
     * Doesn't work if the year is 0 (AKA year of Jesus' birth)
     */
    Entry getNextEntry () {
        Entry current = entries.get(dateCursor);

        double currentDouble = Double.parseDouble(current.getDateCode());

        //Checks if the date is 31st of December (has the form 1999.365)
        if (currentDouble == Math.floor(currentDouble) + 0.365 ){
            //Adds a year
            currentDouble = Math.floor(currentDouble);
            currentDouble += 1;
            //Add a day so it becomes the first of january
            currentDouble += 0.001;

            //If its really close to midnight it might have the form 1999.366
        }else if( currentDouble == Math.floor(currentDouble) + 0.366) {
            currentDouble = Math.floor(currentDouble);
            currentDouble += 1;
            currentDouble += 0.001;
        }else {
            //Not the 31st of Dec
            //adds a day
            currentDouble += 0.001;
        }

        //Default value only if the try block fails
        Date previous = new Date();

        try {

            //Rounds double to prevent float error
            DecimalFormat df = new DecimalFormat("#.######");
            String nextDateCode = df.format(currentDouble);


            //Because of the simpleDataFormat, it interprets  "2017.14" as 2017.14 rather than 2017.140 (it forgets
            // about the hidden trailing zeroes). We must add them our self
            if (nextDateCode.length() < 8){

                if (nextDateCode.length() == 7)
                    nextDateCode += "0";
                else if (nextDateCode.length() == 6)
                    nextDateCode += "00";
                else if (nextDateCode.length()==5)
                    nextDateCode += "000";
            }

            SimpleDateFormat sd = new SimpleDateFormat(Utility.dateCodeFormat);
            System.out.println("Just before parsing next: " + nextDateCode);
            previous = sd.parse(nextDateCode);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Entry entry = getEntry2(previous);


        return entry;

    }



    /**
     * Gets the entry of the day directly before the day indicated by the dateCursor. If one doesn't exist one is made.
     * It works even if the dateCursor is on today
     * Doesn't work if the year is 0 (AKA year of Jesus' birth)
     */
    Entry getPreviousEntry () {
        Entry current = entries.get(dateCursor);

        //Parses the current entry's date code into a double, and one day is subtracted to get the previous day
        double currentDouble = Double.parseDouble(current.getDateCode());


        //Checks if the date is 1st of January (has the form 1999.001)
        if (currentDouble == Math.floor(currentDouble) + 0.001 ){
            //Subtracts the year, and the first of January
            currentDouble -= 1.001;
            //Add a year of days. Essentially this transforms the double to represent the last day of the previous year
            currentDouble += 0.365;

            //If its really close to midnight it might have the form 1999.000
        }else if( currentDouble == Math.floor(currentDouble)) {
            currentDouble -= 1;
            currentDouble += 365;
        }else {
            //Not the 1st of january
            //subtracts a day
            currentDouble -= 0.001;
        }

        //Default value only if the try block fails
        Date previous = new Date();


        try {

            //Rounds double to prevent float error
            DecimalFormat df = new DecimalFormat("#.######");
            String previousDateCode = df.format(currentDouble);



            //Because of the simpleDataFormat, it interprets  "2017.14" as 2017.14 rather than 2017.140 (it forgets
            // about the hidden trailing zeroes). We must add them our self
            if (previousDateCode.length() < 8){

                if (previousDateCode.length() == 7)
                    previousDateCode += "0";
                else if (previousDateCode.length() == 6)
                    previousDateCode += "00";
                else if (previousDateCode.length()==5)
                    previousDateCode += "000";

            }



            SimpleDateFormat sd = new SimpleDateFormat(Utility.dateCodeFormat);
            System.out.println("Just before parsing: "+ previousDateCode);
            previous = sd.parse(previousDateCode);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Entry entry = getEntry2(previous);


        return entry;

    }



    /**
     * @return the entry element referred to by the dateCursor
     */
    Entry getCurrentEntry(){
        return entries.get(dateCursor);
    }


    /**
     * If a given date doesn't already have an entry, a new one is made. Else no effect.
     * To get today's entry call this with a new Date() as parameter. This may be redundent soon
     */
    private void getEntry(Date d){
        loadAllEntries();
        Entry mostRecent = entries.get(0);

        //This entry is used for getDateCode() comparison
        Entry today = new Entry(d);

        if (!mostRecent.getDateCode().equals(today.getDateCode())) {
            //If the most recent entry doesn't have today's dateCode, then add today into storage
            Storage s = new Storage();

            //Writes the new entry into storage and then re-reads storage into entries
            s.write(today);
            loadAllEntries();
        }

    }



    /**
     * @return size of entries
     */
    int getSize () {
        return entries.size();
    }

    public static void main(String[] args) {
//        Journal j = new Journal();
//        Entry toBeWritten = new Entry(new Date());
//        toBeWritten.setMainPassage( "Testing a 1 2 and a 3");
//        j.writeEntry(toBeWritten);
        String tester = "2016.365";
        SimpleDateFormat sd = new SimpleDateFormat(Utility.dateCodeFormat);
        try {
            Date d =sd.parse(tester);
            System.out.println(d);

        } catch (Exception e){
            e.printStackTrace();
        }
        double d = 45.7;
        System.out.println(d);
    }

}
