package model;

/**
 * Created by panduKerr on 20/5/17.
 *
 * This class holds two doubles, x and y.
 */

  public class Coordinates {
    public double x;
    public double y;

    public Coordinates (double x, double y) {
        this.x = x;
        this.y = y;
    }
}
