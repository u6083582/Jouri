package model;

import javax.rmi.CORBA.Util;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.stream.Stream;

/**
 * Created by panduKerr on 20/5/17.
 *
 * Entry represents a data structure. This is the journal entry during assigned to one day (from 12:00 AM to 11:59 PM)
 * The role of this class is to store and update data relating to the entry, not to format it
 */

//TODO: A function that returns the date in a standard format for uniqueness tests
 class Entry {
    private Date date;
    private String mainPassage = "";

    /**
     * This will always create an entry for the current date. It is not up to this class to see if there is already an
     * entry for today
     */
    Entry (){
        date = new Date();
     }

    /**
     * Produces an entry for the input date
     *
     */
    Entry (Date d) {
        date = d;
     }

    /**
     * Replaces mainPassage with passage
     */
    void setMainPassage (String passage){
         mainPassage = passage;
     }

    /**
     * Adds a string to the end of mainPassage
     */
    void appendPassage (String toBeAppended) {
         mainPassage += toBeAppended;
     }

    /**
     * @return the date associated with the passage. The date is in Util.Date's format, so it will be more precise than
     * needs be
     */
    Date getDate (){
         return date;
     }


    String getMainPassage (){
        return mainPassage;
    }

    /**
     * Produces an identifying code for the date, such that every date has a unique code. It must ensure that two entries
     * on the same calendar date produce the same code, but two entries on different dates produce different codes.
     * @return
     */
    String getDateCode(){
        SimpleDateFormat s = new SimpleDateFormat(Utility.dateCodeFormat);
    return s.format(date);
    }

    /**
     * Implements a backspacing operation by copying the current passage into a new, shorter array
     *
     */
    void backSpace (int index) {
//        if (mainPassage.length() > 0){
//            char [] newArray = new char [mainPassage.length() - 1];
//            char [] oldArray = mainPassage.toCharArray();
//            System.arraycopy(oldArray,0,newArray,0,newArray.length);
//            mainPassage = new String (newArray);
//        }
        if (index >  mainPassage.length() || index < 0){
            throw new IndexOutOfBoundsException();
        } else {
            StringBuilder stringBuilder = new StringBuilder(mainPassage);
            stringBuilder.deleteCharAt(index);
            mainPassage = stringBuilder.toString();
        }
    }

    /**
     * Inserts a string at position index in the main passage.
     * Throws IndexOutOfBoundsException if the index given is less than zero or greater than the length of mainPassage
     * @param index - to insert at start, index = 0, to insert at the end, index = this.getMainPassage.getLength()
     */
    void insertStringIntoMainPassageAt (int index, String insert) {

        if (index >  mainPassage.length() || index < 0) {
            throw new IndexOutOfBoundsException();
        } else {

            StringBuilder stringBuilder = new StringBuilder(mainPassage);
            stringBuilder.insert(index, insert);
            mainPassage = stringBuilder.toString();
        }
    }

    @Override
    public String toString() {
        return "dateCode: " + getDateCode() + ", main passage: " + getMainPassage();
    }
}
