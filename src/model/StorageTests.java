package model;

import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import static org.junit.Assert.assertTrue;

/**
 * Created by panduKerr on 21/5/17.
 *
 * Warning: some of these manipulate stored data. Can result in loss of user data
 */
public class StorageTests {

    //TESTS FOR STORAGE
    @Test
    public void isFormatCorrect (){
        Storage storage = new Storage();
        Date d = new Date();
        SimpleDateFormat s = new SimpleDateFormat(Utility.defaultDateFormat);
        assertTrue("original date's toString should be the same as the formatted date, but wasn't"
                        + "\n\ttoString: " + d.toString() + ", formatted: " + s.format(d),
                s.format(d).equals(d.toString()));
    }

    @Test
    public void testRead () {
        Storage s = new Storage();
        Entry e = new Entry();
        e.setMainPassage("testing read.");
        s.write(e);
        Entry e2 = s.read(e.getDateCode());
        assertTrue( "After an Entry is stored and retrieved, it must be identical to the original Entry," +
                        "\n this was not the case. \n\te:  " + e.toString() + "\n\te2: " + e2.toString()
                ,e2.getDate().toString().equals(e.getDate().toString()) && e2.getMainPassage().equals(e.getMainPassage()));
    }

    @Test
    public  void reverseTest() {
        Storage s = new Storage();
        Storage.ObjectAndDouble elem1 = s.new ObjectAndDouble("a", 1.4);
        Storage.ObjectAndDouble elem2 = s.new ObjectAndDouble("b",2);
        Storage.ObjectAndDouble elem3 = s.new ObjectAndDouble("c", 10.11);
        Storage.ObjectAndDouble[] orig = {elem1,elem2,elem3};
        Storage.ObjectAndDouble[] rev = {elem3,elem2,elem1};
        Storage.ObjectAndDouble[] origReversed = s.reverse(orig);
        assertTrue("control reverse: " + Arrays.toString(rev) + ", tested reverse: " + Arrays.toString(origReversed)
                ,Arrays.equals(rev, origReversed));

    }


}
