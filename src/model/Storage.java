package model;


import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by panduKerr on 20/5/17.
 *
 * This class manages the storing and retrieving of persistent storage. The structure in storage consists of a directory
 *  called entries. Each entry is represented by a unique xml file stored in the entries directory. The file's name is an
 *  Entry's dateCode
 */
 class Storage {


    /**
     * Used to interact with this class. It writes an entry into an xml file in the entries directory.
     */
     void write (Entry entry) {
         Document d = createDocument(entry);
         writeEntry(d, entry.getDateCode());
     }


    /**
     * Produces a complete JDOM document
     * @param entry
     */
     Document createDocument (Entry entry){

            //Root element
            Element entryElement = new Element("entry");
            Document doc = new Document(entryElement);

            //Components of an Entry
            Element date = new Element("date");
            date.setText(entry.getDate().toString());

            Element mainPassage = new Element("mainPassage");
            mainPassage.setText(entry.getMainPassage());

            //Adds date and mainPassage to root
            entryElement.addContent(date);
            entryElement.addContent(mainPassage);

        return doc;
    }


    /**
     * Stores a document in entries. If there is another file with the same dateCode, it is overwritten
     * @param dateCode - dateCode of entry in question
     */
    private void writeEntry(Document d, String dateCode) {
        if (Utility.isDateCodeValid(dateCode)){

            File entries = getEntriesDirectory();

            //file is the actual File where the document d is stored
            File file = new File(entries,dateCode);

            try {
                file.createNewFile();


                FileOutputStream fileOutputStream = new FileOutputStream(file);

                //XMLOutputter writes d to fileOutputStream
                XMLOutputter xmlOutput = new XMLOutputter();
                xmlOutput.output(d,fileOutputStream);

                fileOutputStream.close();

            }catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * Searches in the entries directory for a file with the name dateCode. If it does not exist a new Entry is returned,
     * with all fields empty besides a dateCode
     *
     * @param dateCode the dateCode to be searched
     * @return a
     */
    Entry  read (String dateCode) {
        File entries = getEntriesDirectory();
        File entryFile = new File(entries,dateCode);
        //Because of all the try blocks we return entry at the end. It is modified in the try blocks.
        Entry entry = new Entry();

        if (entryFile.exists()) {
            SAXBuilder saxBuilder = new SAXBuilder();
            try {
                //Builds an xml doc from entryFile file
                Document doc = saxBuilder.build(entryFile);

                //Extracts the elements from the doc
                Element entryElement = doc.getRootElement();
                Element dateElement = entryElement.getChild("date");
                Element mainPassageElement = entryElement.getChild("mainPassage");

                //Extract text from each element
                String dateString = dateElement.getText();
                String mainPassageString = mainPassageElement.getText();

                //Parse the dateString into a date
                SimpleDateFormat sd = new SimpleDateFormat(Utility.defaultDateFormat);
                Date date = sd.parse(dateString);

                //Form data into an Entry
                entry = new Entry(date);
                entry.setMainPassage(mainPassageString);

            }catch (Exception e){ e.printStackTrace();}

        } else{
            // There isn't a file called dateCode. We produce a new blank entryFile of the date dataCode

            //Parses the dateCode into a Date. This is then formed as the date for the blank entryFile
            SimpleDateFormat sd = new SimpleDateFormat(Utility.dateCodeFormat);
            Date date = new Date();

            try {
                date = sd.parse(dateCode);
            } catch (Exception e){
                e.printStackTrace();
            }
            entry = new Entry(date);
            write(entry);
        }

        return entry;
    }



    /**
     * Checks if the directory called entries exists. If not it makes the directory.
     * @return the file in the project root, called entries
     */
    private File getEntriesDirectory (){

        File dir = new File("entries");

        //Makes the entries directory if it doesn't exist
        if (!dir.exists()) {
            dir.mkdir();
        }

        return dir;
    }


    /**
     * prints the doc in a pretty format
     * @param d JDOM documet
     */
    private void displayDocument (Document d){

        XMLOutputter xmlOutput = new XMLOutputter();

        xmlOutput.setFormat(Format.getPrettyFormat());
        try {
            String b;
            xmlOutput.output(d, System.out);
        } catch (IOException e){
            e.printStackTrace();
        }
    }


    /**
     * Reads and interprets all files inside entries, returning them as an array of Entries
     * @return an array of all entries from the xml format in the entries directory. It is sorted from largest (most
     * recent) to smallest Entry (defined by date)
     */
    Entry [] readAll () {
        File entriesFile = getEntriesDirectory();
        //Gets an array of Files in entriesFile
        File [] allEntryFiles = entriesFile.listFiles();

        //This will be the return array
        Entry [] entries;

        //In case there are no entriesFile defined
        if (allEntryFiles != null){

                    //On mac there is a hidden file called DS_Store
                    // we need to remove it from the allEntryFiles if it is present.
                    if (hasDS_Store(allEntryFiles)){

                            File [] trimmedFiles = new File[allEntryFiles.length -1];
                            int trimIndex = 0;

                                for (int i = 0; i < allEntryFiles.length; i++) {

                                        if (!allEntryFiles[i].getName().equals(".DS_Store")){
                                            trimmedFiles[trimIndex] = allEntryFiles[i];
                                            trimIndex ++;
                                        }

                                }
                            allEntryFiles = trimmedFiles;
                        }


                //Puts allEntryFiles in order (from largest to smallest
                // the value being a double parsed from the file name)
                allEntryFiles = sort(allEntryFiles);


                entries = new Entry[allEntryFiles.length];


                //reads each entry from persistent memory into entries
                for (int i = 0; i < allEntryFiles.length; i++) {
                    entries [i] = read (allEntryFiles[i].getName());
                }


        } else {
            //There appears to be no Files in entriesFile
            entries = new Entry[0];
        }

        return entries;


    }

    /**
     * On mac there is a hidden file called DS_Store. This is an issue for methods that iterate through all files of a
     * directory
     * @return True if DS_Store is present in the parameter array
     */
    private boolean hasDS_Store (File [] files) {
        Boolean r = false;

        for (int i = 0; i < files.length; i++) {
            if (files [i].getName().equals(".DS_Store")){
                r = true;
                break;
            }
        }

        return r;
    }


    /**
     * Sorts a file whose name can be parsed into a double.
     * @param files - file name must be able to be parsed into a double
     * @return sorted input array (from largest to smallest).
     */
    private File [] sort (File [] files) {
        //Produces an array of ObjectAndDouble allowing it to be sorted
        ObjectAndDouble [] toBeSorted = new ObjectAndDouble[files.length];
        //Uses fileName as the the value for ObjectAndDouble
        for (int i = 0; i < files.length; i++) {
            String name = files [i].getName();

            //Breaks if file name cannot be parsed into a double
            double value = Double.parseDouble(name);

            toBeSorted [i] = new ObjectAndDouble(files [i], value);
        }

        //Sorts the array (smallest to largest), then reverses for correct order
        Arrays.sort(toBeSorted);

        ObjectAndDouble [] sorted = reverse(toBeSorted);

        //Extract the File object from the sorted array
        File [] finalArray  = new File[sorted.length];
        for (int i = 0; i < sorted.length; i++) {
            finalArray[i] = (File) sorted [i].contained;
        }
        return finalArray;
    }

    /**
     * Reverses an array. It does this by copying each element into a new array and returning it. DOES NOT reverse in
     * place
     */
     ObjectAndDouble [] reverse ( ObjectAndDouble [] obDubs) {
        ObjectAndDouble [] newArray = new ObjectAndDouble[obDubs.length];

        //Puts every element into the opposite side of newArray
        int newArrayIndex = obDubs.length - 1;
        for (int i = 0; i < obDubs.length; i++) {
            newArray [newArrayIndex] = obDubs [i];
            newArrayIndex --;
        }

        return newArray;
    }



    /**
     * Holds a Object and a double. The double is used for ordering. This allows for sorting. The Comparator interface
     * is implemented, allowing for sorting by Arrays.sort
     */
     class ObjectAndDouble implements Comparable <ObjectAndDouble> {
        double value;
        Object contained;

        ObjectAndDouble (Object o, double d){
            contained = o;
            value = d;
        }

        @Override
        public int compareTo(ObjectAndDouble o2) {
            double test = this.value - o2.value;
            int result = 0;
            if (test > 0)
                result = 1;
            else if (test < 0)
                result = - 1;
            return result;
        }

        @Override
        public String toString() {
            return "Contained: " + contained.toString() + " value: " + value;
        }
    }

}
