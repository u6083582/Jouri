package model;

import control.KeyCapture;
import display.Main;
import model.Model;

/**
 * Created by panduKerr on 20/5/17.
 *
 * Used to connect the three modules of the program, encouraging encapsulation. Called by Main. Main must set the main
 * field to itself
 */
public class Connection {
//    //Allows for a singleton class
//    static public Connection connector = new Connection();
//
//
//    private Connection (){
//        model = new Model();
//        keyCapture = new KeyCapture(model);
//    }
//
//    public Model model;
//    public KeyCapture keyCapture;
//    public Main main;
}
