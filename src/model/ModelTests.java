package model;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by panduKerr on 20/5/17.
 */
public class ModelTests {

    //TESTS FOR JOURNAL
    @Test
    public void testMakesDefaultEntry (){
        Journal j = new Journal();
        assertTrue("Journal was instantiated but did note make an entry",j.getSize() > 0);
    }

    @Test
    public void testLoadAllEntries (){}


    //TESTS FOR ENTRY.

    // This test won't work around midnight
    @Test
    public void testDateCodeSame () {
        Entry one = new Entry();
        Entry two = new Entry();
        assertTrue("Two dates were meant to have same date codes, but they were different"
                , one.getDateCode().equals(two.getDateCode()));
    }

    //Tests for correct length
    @Test
    public void testDateCodeInBounds () {
        Entry one = new Entry();
        assertTrue("date code shoud have length 8, but doesn't",one.getDateCode().length() == 8);
    }


    @Test
    public void testBackSpace (){
        Entry one = new Entry();
        one.setMainPassage("Testing, one, two, three");
        int oldLength = one.getMainPassage().length();
        one.backSpace(one.getMainPassage().length());
        assertTrue("The new passage should be one less in length than the old passage but isn't."
                +"old length: " + oldLength +", new length: " + one.getMainPassage().length()
                , oldLength  == 1 + one.getMainPassage().length());
    }

    @Test
    public void testBackSpaceEmpty () {
        Entry one = new Entry();
        one.setMainPassage("");
        one.backSpace(0);
        assertTrue("Using backspace() on an empty mainPassage should result in no change, but there was",
                one.getMainPassage().equals(""));

    }
    @Test
    public void testAppendPassage () {
        Entry one = new Entry();
        String initialString = "Initial";
        one.setMainPassage(initialString);
        one.appendPassage("123");
        assertTrue("Main passage should have increased in length of 3, but didn't"
                , initialString.length() + 3 == one.getMainPassage().length() );
    }

    @Test
    public void testInsertEmpty () {
        Entry one = new Entry();
        String test = "Cat dog dog";
        one.insertStringIntoMainPassageAt(0, test);
        assertTrue("Insertion failed, expected passage: " + test +", resulted passage: " + one.getMainPassage()
                , one.getMainPassage().equals(test));
    }

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void testInvalidIndexToLarge () {
        exception.expect(IndexOutOfBoundsException.class);

        Entry one = new Entry();
        String test = "Cat dog dog";
        one.insertStringIntoMainPassageAt(5, test);
    }

    @Test
    public void testInvalidIndexNegative () {
        exception.expect(IndexOutOfBoundsException.class);

        Entry one = new Entry();
        String test = "Cat dog dog";
        one.insertStringIntoMainPassageAt(-1, test);
    }

    @Test
    public void testInsertMiddle () {
        Entry one = new Entry();
        String initial = "abcdefgh";
        one.setMainPassage(initial);

        String toBeInserted = "HELLO";
        String test = "abcHELLOdefgh";

        one.insertStringIntoMainPassageAt(3, toBeInserted);
        assertTrue("Insertion failed, expected passage: " + test +", resulted passage: " + one.getMainPassage()
                , one.getMainPassage().equals(test));
    }


    @Test
    public void testInsertEnd () {
        Entry one = new Entry();
        String initial = "abcdefgh";
        one.setMainPassage(initial);

        String toBeInserted = "HELLO";
        String test = "abcdefghHELLO";

        one.insertStringIntoMainPassageAt(one.getMainPassage().length(), toBeInserted);
        assertTrue("Insertion failed, expected passage: " + test +", resulted passage: " + one.getMainPassage()
                , one.getMainPassage().equals(test));
    }





}
